package edu.sise.entradaalcine;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class InicioActivity extends AppCompatActivity {
    public static final String TAG="InicioActivity";

    EditText edtNombres, edtApellidos, edtDni,edtEmail;
    Spinner spPeliculas;
    RadioButton rbMasculino, rbFemenino;
    Button btnRegistrar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,">>>método onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        this.initObjects();
    }

    protected void initObjects(){
        Log.d(TAG,">>>Inicializando initObjects()");
        edtNombres = findViewById(R.id.edtNombres);
        edtApellidos = findViewById(R.id.edtApellidos);
        edtDni = findViewById(R.id.edtDni);
        edtEmail = findViewById(R.id.edtEmail);
        spPeliculas = findViewById(R.id.spPeliculas);
        rbMasculino = findViewById(R.id.rbMasculino);
        rbFemenino = findViewById(R.id.rbFemenino);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
    }

    public void onClickRegistrar(View view){
        Log.d(TAG,">>>Metodo onClickRegistrar()");

        String nombres = edtNombres.getText().toString();
        String apellidos = edtApellidos.getText().toString();
        String dni = edtDni.getText().toString();

        Toast.makeText(this, "Hola "+nombres+" "+apellidos, Toast.LENGTH_SHORT).show();

    }

    public void onClickLimpiar(View view){
        Log.d(TAG,">>>Metodo onClickLimpiar()");

    }
}
